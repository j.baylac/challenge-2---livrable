# POUR IMPORTER LES PLUGINS DONT NOUS AVONS BESOIN
import webbrowser
import json

# PREMIERE FONCTION QUI CHARGE LES DONNEES DU FICHIER JSON ET DE LES STOCKER DANS UN DICTIONNAIRE GENERE
def jsonToArray(dico):
    with open(dico) as stockFile:
        json2Dico = json.load(stockFile)

    # POUR CREER UN TABLEAU (INVISIBLE)
    array = []

    # POUR CREER UNE BOUCLE ET AJOUTER NOS CIBLES "PARENTS" EN INDIQUANT QU'ELLES DOIVENT ETRE
    # ACCOMPAGNEES DU NOM ET DE L'HEXADECIMALE DE CHAQUE COULEURS
    for color in json2Dico:
        parents = ('parent', color['name'], color['hex'])
        array.append(parents)

    # POUR CREER UNE BOUCLE ET AJOUTER NOS NOS CIBLES "ENFANTS" EN INDIQUANT QU'ELLES DOIVENT
    # ETRE ACCOMPAGNEES DU NOM, DE L'HEXADECIMAL ET DE LA COMPAGNIE DE CHAQUE COULEURS

        for color_child in color['children']:
            childs = (color_child['name'], color_child['hex'], color_child['company'])
            array.append(childs)


# POUR QU'A LA FIN DE LA BOUCLE IL NOUS RETOURNE LES RESULTATS DES RECHERCHES PARENTS ET ENFANTS A PARTIR DU TABLEAU
    return array
    stockFile.close()
#POUR COMPTER LE NOMBRE DE COULEURS (PARENTS).
def getTotal(dico, childrens):
    with open(dico) as stockFile:
        json2Dico = json.load(stockFile)

    return len(json2Dico)
    stockFile.close()
# POUR NUMEROTER (LISTER) ET ASSIGNER LES LIGNES DE NOTRE TABLEAU AFIN DE POURVOIR LES INTRODUIRE PAR LA SUITE DANS NOTRE CODE HTML (PLUS BAS)
def arrayToString(array):
    for arrayLine in array:
        print(arrayLine[0]+' '+arrayLine[1]+' '+arrayLine[2])

# POUR CREER UNE BOUCLE INCLUANT LES RESULTAS SE TROUVANT DANS LIGNES DU TABLEAU (PLUS HAUT) ET DE LES INTROUDIRE DANS LE CODE HTML (JUSTE EN BAS)
def arrayToHtml(array):
    colorHtml=''
    for arrayLine in array:
        colorHtml+='\
        <div class="bloc '+arrayLine[0].replace(" ","")+'">\
            '+arrayLine[0]+'\
        </div>'
    return '<!DOCTYPE html>\
    <html>\
        <head>\
            <link href="style.css" rel="stylesheet" type="text/css"/>\
        </head>\
    <body>\
    <div> Ma belle palette de couleur !</div>\
    <div>'+ colorHtml + '\</div>\
    </body>\
    </html>'


# POUR QUE LE CODE HTML SAISIE DANS array2Html SOIT ECRIT (write) DANS NOTRE FUTUR FICHIER HTML
def htmlToFile(filename, htmlString):
    poire = open(filename, "w")
    poire.write(htmlString)

    # POUR DEMANDER UN PRINT FINAL DES RESULTATS
    finalResult = jsonToArray('colors.json')


    # POUR QUE LE CODE HTML ET LE RESULTAT DE LA FONCTION array2Html SOIENT GENERES DANS LE FUTUR FICHIER challenge2.html
    htmlToFile("challenge2.html", arrayToHtml(finalResult))

    # POUR QUE challenge2.py OUVRE AUTOMATIQUEMENT DANS UN NOUVEL ONGLET DU NAVIGATEUR LE FUTUR FICHIER challenge2.html
    futurHtmlFile = 'challenge2.html'
    return finalResult
    poire.close()
    webbrowser.open_new_tab(futurHtmlFile)

def arrayToCSS(array):
    colorCSS=''
    for arrayLine in array:
        colorCSS+='\
        .'+arrayLine[0].replace(" ","")+'{background-color :'+arrayline[1]+'}\
        '
    return ' a { background-color:Gainsboro; margin-bottom:30px; padding:10px;}\
    '+ colorCSS +'\
    '


# POUR QUE LE CODE HTML SAISIE DANS array2Html SOIT ECRIT (write) DANS NOTRE FUTUR FICHIER HTML
def CSSToFile(filename, CssString):
    figue = open(filename, "w")
    figue.write(CssString)

    # POUR DEMANDER UN PRINT FINAL DES RESULTATS
    finalResultCSS = jsonToArray('colors.json')


    # POUR QUE LE CODE HTML ET LE RESULTAT DE LA FONCTION array2Html SOIENT GENERES DANS LE FUTUR FICHIER challenge2.html
    CSSToFile("style.css", arrayToCSS(finalResultCSS))

    # POUR QUE challenge2.py OUVRE AUTOMATIQUEMENT DANS UN NOUVEL ONGLET DU NAVIGATEUR LE FUTUR FICHIER challenge2.html
    futurCSSFile = 'style.css'
    return finalResultCSS
    figue.close()
